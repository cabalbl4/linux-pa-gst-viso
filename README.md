# linux-pa-gst-viso

Gstreamer audio visualizations with pulseaudio using a simple bash script

Because most audio players don't do it (right) anymore

![Visualizations example](http://journals.ru/attach/177/17656/1020067.jpg)
![Menu example](http://journals.ru/attach/177/17656/1020066.jpg)

## usage

Just run script and enjoy graphical config with zenity (if all deps are there).

## requirements

* Gstreamer 1.0 with tools (includes gst-launch). Ubuntu `sudo apt-get install gstreamer1.0-tools` (version 0.10 see [this my old post](https://ubuntuforums.org/showthread.php?t=1673559))
* zenity. Ubuntu  `sudo apt-get install zenity`
* libvisual (libvisual-0.4 is recent, maybe other will work too). Ubuntu `sudo apt-get install libvisual-0.4-0 libvisual-0.4-plugins`
* pulseaudio
* bash (tested in v5.0.17, but shoud work with older versions too)

## recent features

* monitor autodetect


## disclaimer

Just saving my local script here so it can be useful. :)

# install for local user (/home/\<user\>/bin)

` (mkdir ~/bin || true) && curl https://gitlab.com/cabalbl4/linux-pa-gst-viso/-/raw/master/visuals.sh > ~/bin/visuals.sh && chmod +x ~/bin/visuals.sh
`

## license (MIT)

Copyright (c) 2021 Ivan Vokhmin (Cabalbl4)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
