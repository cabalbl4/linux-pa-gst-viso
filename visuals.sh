#!/bin/bash
############################
# Written by Ivan Vokhmin (Cabalbl4) <cabalbl4@gmail.com>
############################
detectedmonitors=$(pactl list| grep monitor | grep Source | cut -f 2 -d ':' | sed 's/ //' | xargs -I {} echo '{} {}')
monselect=$(zenity --list --width="480" --height="240" --title="Select audio monitor" \
--column="Monitor" --column="Description" \
$detectedmonitors \
"Custom monitor" "Set this value inside script" \
"Default monitor" "Non-specific monitor" \
)
echo "USING MONITOR:" $monselect
case $monselect in
"Custom monitor")
################################################
#Custom monitor goes here, change 'device=...' according to the output of pactl list|grep monitor
mymon='bluez_sink.D8_1D_FF_D9_26_C8.a2dp_sink.monitor'
################################################
device="device=$mymon"
;;
"Default monitor")

mymon='' 
device=''
;;
*)
if [ -z "$monselect" ]
then
    zenity --error --text='No monitor selected. Using default.'
    mymon='' 
    device=''
else
    mymon=$monselect
    device="device=$mymon"
fi

esac

preset=$(zenity --list --width="480" --height="500" --title="Select Plugin" --print-column="2" \
--column="Num" --column="Library name" --column="Description" \
"1" "libvisual_bumpscope" "Bumpscope visual plugin" \
"2" "libvisual_corona"    "Libvisual corona plugin" \
"3" "libvisual_infinite" "Infinite visual plugin" \
"4" "libvisual_jakdaw"    "Jakdaw visual plugin" \
"5" "libvisual_jess"    "Jess visual plugin" \
"6" "libvisual_lv_analyzer" "Libvisual analyzer plugin" \
"7" "libvisual_lv_scope" "Libvisual scope plugin" \
"8" "libvisual_oinksie"    "Libvisual Oinksie visual plugin" \
"9" "goom" "goom plugin" \
"10" "goom2k1" "goom2k1 plugin" \
"11" "monoscope" "Monoscope plugin" \
"12" "spacescope" "Simple stereo visualizer" \
"13" "spectrascope" "Simple frequency spectrum scope" \
"14" "wavescope" "Simple waveform oscilloscope" \
"15" "synaescope" "Creates video visualizations of audio input using stereo and pitch information") 

if [ -z "$preset" ]
then
    zenity --error --text='Cancelled'
else
    gst-launch-1.0 pulsesrc  $device ! $preset ! videoconvert ! xvimagesink force-aspect-ratio=false
fi

